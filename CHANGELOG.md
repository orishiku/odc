# Change Log

## [2023.3.16] 2023-07-08

### Changed
- updated dacommon version

## [2023.3.14] 2023-06-27

### Changed
- updated dapricot version
- minor style fixes

## [2023.3.12] 2023-06-22

### Added
- 'cycling/race' page for integration app mode demo

## [2023.3.12] 2023-06-17

### Added
- new dependency: django-reset-migrations
 
## [2023.3.11] 2023-06-15

### Changed
- updated dapricot suite

## [2023.3.9]

### Changed
- code page style fixes

## [2023.3.8]

### Changed
- minor style fixes

### Added
- sections to menu
- spotify latest tops

## [2023.3.7]

### Added
- custom logger

## [2023.3.0]

### Added
- pages: code, cycling, music
- some style

## [2023.2.1]

### Added
- Gitlab CI/CD pipeline settings
- Pipeline tests and code analysis

### Changed
- Dependencies updated

## [2023.2.0]

### Added
- changelog file
- configuration for caprover

### Removed
- dajournal and damusic from dependencies

### Changed
- CI pipelines moved to Jenkins. CD stays on gitlab-ci
- Requeriments management moved to Poetry
- static settings for s3 data persistency
