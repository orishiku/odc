""" Storages module
"""
from django.core.files.storage import get_storage_class
from storages.backends.s3boto3 import S3Boto3Storage


class StaticStorage(S3Boto3Storage):
    """
    S3 storage backend that saves the files locally, too.
    """

    location = "static"

    def get_accessed_time(self, name):
        """get_accessed_time"""

    def get_created_time(self, name):
        """get_created_time"""

    def path(self, name):
        """path"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage"
        )()

    def save(self, name, content):
        self.local_storage.save(name, content)
        super().save(name, self.local_storage._open(name))
        return name


class PublicMediaStorage(S3Boto3Storage):
    """
    S3 storage backend for media
    """

    location = "media"
    default_acl = "public-read"
    file_overwrite = False

    def get_accessed_time(self, name):
        """get_accessed_time"""

    def get_created_time(self, name):
        """get_created_time"""

    def path(self, name):
        """path"""
