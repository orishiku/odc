""" Views module
"""
from dacommon.views import BaseView
from dacommon.integrations.bases import BaseAppView
from dacommon.utils.datetime import get_period, PeriodName
from dacommon.templatetags.dacommon import get_user

from dafitness.integrations.strava import StravaIntegration, Scopes


class HomeView(BaseView):
    """Home view"""

    template_name = "home.html"

    def dispatch(self, request, *args, **kwargs):
        self.add_data("site_title", "Orishiku dot com")
        return BaseView.dispatch(self, request, *args, **kwargs)


class CodeView(BaseView):
    """Home view"""

    template_name = "code.html"

    def dispatch(self, request, *args, **kwargs):
        self.add_data("site_title", "Orishiku dot com")
        return BaseView.dispatch(self, request, *args, **kwargs)


class CyclingView(BaseView):
    """Home view"""

    template_name = "cycling.html"

    def dispatch(self, request, *args, **kwargs):
        self.add_data("site_title", "Orishiku dot com")
        return BaseView.dispatch(self, request, *args, **kwargs)


class MusicView(BaseView):
    """Home view"""

    template_name = "music.html"

    def dispatch(self, request, *args, **kwargs):
        self.add_data("site_title", "Orishiku dot com")
        return BaseView.dispatch(self, request, *args, **kwargs)


class RaceMeView(BaseAppView):
    """Home view"""

    template_name = "race_me.html"
    integration_class = StravaIntegration
    integration_scopes = [
        Scopes.ACTIVITY_READ,
    ]

    def response_data(self, request):
        from_date, to_date = get_period(period_unit=PeriodName.WEEK)
        strava = StravaIntegration(
            request,
            [Scopes.ACTIVITY_READ_ALL, Scopes.PROFILE_READ_ALL],
            user=get_user("admin"),
        )
        me_stats = strava.total_stats(from_date=from_date, to_date=to_date)
        contender_stats = self.integration.total_stats(
            from_date=from_date, to_date=to_date
        )
        contender_stats["user"] = self.integration.athlete["username"]

        self.template_variables["race_info"] = {
            "me": me_stats,
            "contender": contender_stats,
        }

    def dispatch(self, request, *args, **kwargs):
        self.add_data("site_title", "Orishiku dot com")
        return BaseView.dispatch(self, request, *args, **kwargs)
