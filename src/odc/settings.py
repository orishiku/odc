""" Base settings module
"""
import os
from pathlib import Path

import logging.config
from django.utils.log import DEFAULT_LOGGING
import dj_database_url
from dacommon.utils import get_stylesheets_sources, get_project_scripts

BASE_DIR = Path(__file__).resolve().parent.parent
SECRET_KEY = str(os.getenv("DJANGO_SECRET_KEY"))
DEBUG = os.getenv("DJANGO_DEBUG") != "False"
ALLOWED_HOSTS = str(os.getenv("DJANGO_ALLOWED_HOSTS")).split(",")

if not DEBUG:
    CSRF_TRUSTED_ORIGINS = str(os.getenv("CSRF_TRUSTED_ORIGINS")).split(",")

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "reset_migrations",
    "storages",
    "compressor",
    "dapricot",
    "dapricot.accounts",
    "dacommon",
    "dafitness",
    "damusic",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "dafitness.middleware.IntegrationsMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "odc/templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.static",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

ROOT_URLCONF = "odc.urls"

WSGI_APPLICATION = "odc.wsgi.application"

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

LANGUAGE_CODE = "en-us"
TIME_ZONE = "America/Mexico_City"
USE_I18N = True
USE_TZ = True

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
DATABASES = {
    "default": dj_database_url.config()
    if not DEBUG
    else {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

STATIC_ROOT = BASE_DIR / "static"
STATICFILES_DIRS = [
    BASE_DIR / "odc/static",
]

if DEBUG:
    MEDIA_URL = "media/"
    MEDIA_ROOT = BASE_DIR / "media"
    STATIC_URL = "static/"

else:
    AWS_ACCESS_KEY_ID = os.getenv("MINIO_ACCESS_KEY")
    AWS_SECRET_ACCESS_KEY = os.getenv("MINIO_SECRET_KEY")
    AWS_STORAGE_BUCKET_NAME = os.getenv("MINIO_BUCKET_NAME")
    AWS_S3_ENDPOINT_URL = os.getenv("MINIO_ENDPOINT")
    AWS_S3_OBJECT_PARAMETERS = {"CacheControl": "max-age=86400"}
    AWS_DEFAULT_ACL = None
    AWS_QUERYSTRING_AUTH = False
    AWS_S3_FILE_OVERWRITE = False

    COMPRESS_URL = f"{AWS_S3_ENDPOINT_URL}/{AWS_STORAGE_BUCKET_NAME}/static/"
    STATIC_URL = COMPRESS_URL
    STATICFILES_STORAGE = "odc.storages.StaticStorage"
    COMPRESS_STORAGE = STATICFILES_STORAGE
    MEDIA_URL = f"{AWS_S3_ENDPOINT_URL}/{AWS_STORAGE_BUCKET_NAME}/media/"
    DEFAULT_FILE_STORAGE = "odc.storages.StaticStorage"
    COMPRESS_OFFLINE_MANIFEST = "static_manifest.json"
    COMPRESS_OFFLINE_MANIFEST_STORAGE = STATICFILES_STORAGE
    COMPRESS_ROOT = STATIC_ROOT

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
)

COMPRESS_PRECOMPILERS = (("text/x-scss", "django_libsass.SassCompiler"),)
STYLESHEETS_SOURCES = get_stylesheets_sources("scss/base.scss")
SCRIPTS_SOURCES = get_project_scripts("js/site.js")

COMPRESS_ENABLED = True
COMPRESS_OFFLINE = os.getenv("COMPRESS_OFFLINE") != "False"

COMPRESS_OFFLINE_CONTEXT = {
    "stylesheets_sources": STYLESHEETS_SOURCES,
    "scripts_sources": SCRIPTS_SOURCES,
}
SITE_ID = 2
# DAACOUNTS
AUTH_USER_MODEL = "daaccounts.User"
AUTH_USERNAME_FIELD = "username"
AUTO_SIGNUP_ENABLED = False
ACCOUNTS_REQUESTS_ENABLED = False
LOGIN_REDIRECT_URL = "home"
LOGOUT_REDIRECT_URL = "home"
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

# Disable Django's logging setup
LOGGING_CONFIG = None

LOGLEVEL = os.environ.get("LOGLEVEL", "info").upper()

logging.config.dictConfig(
    {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "default": {
                # exact format is not important, this is the minimum information
                "format": "%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
            },
            "django.server": DEFAULT_LOGGING["formatters"]["django.server"],
        },
        "handlers": {
            # console logs to stderr
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "default",
            },
            "django.server": DEFAULT_LOGGING["handlers"]["django.server"],
        },
        "loggers": {
            # default for all undefined Python modules
            "": {
                "level": "WARNING",
                "handlers": ["console"],
            },
            # Our application code
            "app": {
                "level": LOGLEVEL,
                "handlers": ["console"],
                # Avoid double logging because of root logger
                "propagate": False,
            },
            # Default runserver request logging
            "django.server": DEFAULT_LOGGING["loggers"]["django.server"],
        },
    }
)
