""" Base urls module
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from django.conf.urls.static import static
from django.conf import settings

from odc.views import HomeView, CyclingView, MusicView, CodeView, RaceMeView

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("code/", CodeView.as_view(), name="code"),
    path("admin/", admin.site.urls),
    path("accounts/", include("dapricot.accounts.urls")),
    path("cycling/", CyclingView.as_view(), name="cycling"),
    path("cycling/race", RaceMeView.as_view(), name="race_me"),
    path("cycling/", include("dafitness.urls")),
    path("music/", MusicView.as_view(), name="music"),
    path("music/", include("damusic.urls")),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
