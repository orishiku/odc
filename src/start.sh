#!/bin/sh
# Django setup
# python manage.py reset_migrations dapricot dacommon daaccounts dafitness damusic;
python manage.py makemigrations dapricot dacommon daaccounts dafitness damusic;
python manage.py migrate --no-input;
python manage.py collectstatic --noinput --clear;

if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] && [ -n "$DJANGO_SUPERUSER_EMAIL" ];then
	python manage.py createsuperuser --no-input --email $DJANGO_SUPERUSER_EMAIL
fi

# Run server
/.venv/bin/gunicorn -b :8000 odc.wsgi:application